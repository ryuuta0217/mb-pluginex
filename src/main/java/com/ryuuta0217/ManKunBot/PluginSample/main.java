package com.ryuuta0217.ManKunBot.PluginSample;

import com.ryuuta0217.ManKunBot.Plugin.ManKunPlugin;

public class main extends ManKunPlugin {
    private com.ryuuta0217.ManKunBot.PluginSample.testListener testListener = new testListener();

    @Override
    public void onEnable() {
        getCommandClient().addCommand(new testCommand());
        getJDA().addEventListener(testListener);
    }

    @Override
    public void onDisable() {
        getJDA().removeEventListener(testListener);
    }
}
