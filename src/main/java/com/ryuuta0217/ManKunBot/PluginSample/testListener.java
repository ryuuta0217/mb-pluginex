package com.ryuuta0217.ManKunBot.PluginSample;

import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class testListener extends ListenerAdapter {
    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        if(event.getMessage().getMentionedMembers().contains(event.getGuild().getSelfMember())) {
            event.getChannel().sendMessage(":relaxed:").queue();
        }
    }
}
