package com.ryuuta0217.ManKunBot.PluginSample;

import com.jagrosh.jdautilities.command.CommandEvent;
import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import com.ryuuta0217.ManKunBot.Commands.OwnerCommand;

public class testCommand extends OwnerCommand {
    public testCommand() {
        this.name = "test";
        this.guildOnly = false;
    }
    public void doCommand(CommandEvent event, EventWaiter waiter) {
        event.reply("こんにちは! :)");
    }
}
